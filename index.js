// "document" refers to the whole webpage
// "querySelector" is used to select a specific object (HTML element) from our document (webpage)
const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

// document.getElementByID()
// document.getElementByClassName()
// const txtFirstName = document.getElementByID("#txt-first-name");
// const spanFullName = document.getElementByID("#span-full-name");

// Whenever a user interacts with a web page, this action is considered as an event
txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value;
	console.log(event.target);
	console.log(event.target.value);
});

txtLastName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
	console.log(event.target);
	console.log(event.target.value);
});

